# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""
# from http://www.python-excel.org
import xlwt
import csv
import sys

def decodeDay(day):
    if day == 'MO':
        dayInt = 0
    elif day == 'TU':
        dayInt = 1
    elif day == 'WE':
        dayInt = 2
    elif day == 'TH':
        dayInt = 3
    elif day == 'FR':
        dayInt = 4
    else:
        dayInt = 5
    return dayInt

def getTime(timeslot):
    if len(timeslot) == 0:
        startTime = 0
        endTime = 0
    else:
        startTime, endTime = timeslot.split('-')
        time = startTime.split(':')
        startTime = int((int(time[0]) - 8) * 2 + int(time[1]) / 30)
        if startTime > 18:
            startTime = 18
        time = endTime.split(':')
        endTime = int((int(time[0]) - 8) * 2 + int(time[1]) / 30)
        if endTime > 18:
            endTime = 19
        
    return [i for i in range(startTime, endTime)]

def printSchedule(entryList, instructor, workbook):
    ws = workbook.add_sheet(instructor)
    style = xlwt.XFStyle()
    style.alignment.wrap = 1
    style.alignment.horz = xlwt.Alignment.HORZ_CENTER

    ws.write(0, 0, instructor)
    headrow = 1
    ws.write(headrow, 0, 'วัน/เวลา')
    for c in range(1, 10):
        ws.write_merge(headrow, headrow, c*2-1, c*2, str(c+7)+':00-'+str(c+8)+':00', style)
    ws.write(headrow, 19, '18:00-21:00', style)

    contentrow = headrow + 1
    for day in {'MO', 'TU', 'WE', 'TH', 'FR', 'AR'}:
        ws.write(contentrow + decodeDay(day), 0, day)

    rows = [['' for i in range(0, 20)] for j in range(0, 6)]

    for entry in entryList:
        for day in entry['Meeting Day']:
            timeSlot = getTime(entry['Meeting Time'])
            daySlot = decodeDay(day)
            if daySlot >= 5 or len(timeSlot) == 0:
                rows[5][0] += ' '+entry['Catalog Number']
            else:
                for time in timeSlot:
                    rows[daySlot][time] += entry['Catalog Number']
                    
                    if len({item['Section']
                        for item in datamap if item['Catalog Number'] == entry['Catalog Number']}) > 1:
                            rows[daySlot][time] += ' ('+entry['Section']+')'

                    rows[daySlot][time] += '\n'+entry['Room']+'\n'

    for r in range(0, len(rows)):
        for c in range(0, len(rows[r])):
            ws.write(contentrow + r, c+1, rows[r][c].strip(), style)
    return

with open(sys.argv[1], encoding='tis-620') as f:
    data = [row for row in csv.reader(f, delimiter=',', skipinitialspace=True)]

# last line is <script></script>, it must be deleted    
data = data[0:-1]

datamap = [{data[0][i]:data[j][i] for i in range(0, len(data[0]))} for j in range(1, len(data))]

for item in datamap:
#    entry['Catalog Number'] = entry['Catalog Number'][4:]
    item['Meeting Day'] = item['Meeting Day'].split(' ')
    item['Instructor'] = set(item['Instructor'].split(','))

instructorSet = {person for entry in datamap for person in entry['Instructor'] if person is not ''}
instructorList = sorted(list(instructorSet))

workbook = xlwt.Workbook()
for instructor in instructorList:
    print(instructor)
    itemList = [entry for entry in datamap if instructor in entry['Instructor']]
    printSchedule(itemList, instructor, workbook)

workbook.save('schedule.xls')
    