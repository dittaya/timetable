# README #

The script is for generating class schedule for each instructor


### Input ###
Currently, it supports only CSV format (extracted from CU-SAA)

### Required fields in an input file ###
Term, Catalog Number,Course Title,Section,Teach Type,Meeting Day,Meeting Time,Room,Instructor,Remarks,Capacity,Class Status,Final Exam Date,Final Start Time,Final End Time,Midterm Exam Date,Midterm Start Time,Midterm End Time

### Output ###
* schedule.xlsx (MS Excel 2010 file)
* room.xlsx (MS Excel 2010 file)

### Setup ###

# Dependencies #
* openpyxl 2.0.2-py34_0
* Python 3.4

# Run the script #
python schedule.py <inputfile>