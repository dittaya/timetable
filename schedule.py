# -*- coding: utf-8 -*-
"""
Created on Fri Aug  7 15:55:20 2015

@author: dittaya
Dependencies:
- openpyxl 2.0.2-py34_0

CHANGE LOG:
2015-08-26
- Add timetable for room use
2015-08-07
- change from xlwt to openpyxl 2.0.x
- add page setup and margins
KNOWN BUGS:
- LibreOffice cannot detect auto fit width and height
- Excel 2010 cannot set page margins
"""


from openpyxl import Workbook
from openpyxl.styles import Alignment, Font, Style
from openpyxl.styles.borders import Border, Side
from openpyxl.cell import get_column_letter
import datetime
import csv
import sys

def decodeDay(day):
    if day == 'MO':
        dayInt = 0
    elif day == 'TU':
        dayInt = 1
    elif day == 'WE':
        dayInt = 2
    elif day == 'TH':
        dayInt = 3
    elif day == 'FR':
        dayInt = 4
    else:
        dayInt = 5
    return dayInt

def getTime(timeslot):
    if len(timeslot) == 0:
        startTime = 0
        endTime = 0
    else:
        startTime, endTime = timeslot.split('-')
        time = startTime.split(':')
        startTime = int((int(time[0]) - 8) * 2 + int(time[1]) / 30)
        if startTime > 18:
            startTime = 18
        time = endTime.split(':')
        endTime = int((int(time[0]) - 8) * 2 + int(time[1]) / 30)
        if endTime > 18:
            endTime = 19

    return [i for i in range(startTime, endTime)]

def printSchedule(datamap, instructor, workbook):
    entryList = [entry for entry in datamap if instructor in entry['Instructor']]

    ws = workbook.create_sheet()

    # set sheet name
    ws.title = instructor

    # cell alignment
    alignment=Alignment(horizontal='center',
                        vertical='center',
                        wrap_text=True,
                        shrink_to_fit=True)

    # cell border
    thinBorder = Border(left=Side(style='thin'),
                         right=Side(style='thin'),
                         top=Side(style='thin'),
                         bottom=Side(style='thin'))

    # cell font
    font = Font(size=8)

    style = Style(alignment=alignment, font=font, border=thinBorder)

    for r in range(2,9):
        for c in range(1, 21):
            cell = ws.cell(row=r, column=c)
            cell.style = style

    row = 1
    cell = ws.cell(row=1, column=1)
    cell.value = instructor
    cell.style = cell.style.copy(font=Font(bold=True))

    row = row + 1
    headrow = row

    cell = ws.cell(row=headrow, column=1)
    cell.value = 'วัน/เวลา'

    for c in range(1, 10):
        ws.merge_cells(start_row=headrow-1, start_column=c*2-1, end_row=headrow-1, end_column=c*2)
        cell = ws.cell(row=headrow, column=c*2)
        cell.value = str(c+7)+':00-'+str(c+8)+':00'
    cell = ws.cell(row=headrow, column=20)
    cell.value = '18:00-21:00'

    contentrow = headrow + 1
    for day in {'MO', 'TU', 'WE', 'TH', 'FR', 'AR'}:
        cell = ws.cell(row=contentrow + decodeDay(day), column=1)
        cell.value = day

    rows = [['' for i in range(0, 20)] for j in range(0, 6)]

    term = entryList[0]['Term']
    year = term[1:3]
    if term[3] == '1':
        semester = 'ต้น'
    elif term[3] == '2':
        semester = 'ปลาย'
    elif term[3] == '3':
        semester = 'ฤดูร้อน'

    for entry in entryList:
        for day in entry['Meeting Day']:
            timeSlot = getTime(entry['Meeting Time'])
            daySlot = decodeDay(day)
            if daySlot >= 5 or len(timeSlot) == 0:
                rows[5][0] += ' '+entry['Catalog Number']
            else:
                for time in timeSlot:
                    rows[daySlot][time] += entry['Catalog Number']

                    if len({item['Section']
                        for item in datamap if item['Catalog Number'] == entry['Catalog Number']}) > 1:
                            rows[daySlot][time] += ' ('+entry['Section']+')'

                    rows[daySlot][time] += '\n'+entry['Room']+'\n'

    for r in range(0, len(rows)):
        for c in range(0, len(rows[r])):
            cell = ws.cell(row=contentrow + r, column=c+2)
            cell.value = rows[r][c].strip()

    # print instructor list
    notestyle = Style(font=font)
    courseList = dict()
    for entry in entryList:
        catalogNumber = entry['Catalog Number']
        courses = [course for course in datamap if course['Catalog Number'] == catalogNumber]
        for course in courses:
            section = course['Section']
            if (catalogNumber, section) not in courseList:
                courseList[catalogNumber, section] = course['Instructor']
            else:
                courseList[catalogNumber, section] |= course['Instructor']                
    
    lastRow = contentrow + len(rows) + 1
    lastColumn = 2
    keys = sorted(courseList.keys())
    for catalogNumber,section in keys:
        cell = ws.cell(row=lastRow, column=lastColumn)
        cell.style = notestyle
        cell.value = catalogNumber
        cell = ws.cell(row=lastRow, column = lastColumn+1)
        cell.style = notestyle
        cell.value = 'Section '+section
        cell = ws.cell(row=lastRow, column = lastColumn+3)
        cell.style = notestyle
        cell.value = ','.join(courseList[catalogNumber,section])
        lastRow += 1
        if lastRow == 16:
            lastRow = contentrow+ len(rows) + 1
            lastColumn += 6

    # landscape A4 paper and fit to width
    ws.page_setup.orientation = ws.ORIENTATION_LANDSCAPE
    ws.page_setup.paperSize = ws.PAPERSIZE_A4
    ws.page_setup.fitToWidth = 1
    ws.page_setup.fitToHeight = 1

    # Margin left,right = 0.2 inches, top,bottom = 0.75 inches
#    ws.page_margins.left = 0.2
#    ws.page_margins.right = 0.2
#    ws.page_margins.top = 0.5
#    ws.page_margins.bottom = 0.5
#    ws.page_margins.header = 0.25

    # column width
    ws.column_dimensions[get_column_letter(1)].width = 5
    for c in range(2, 20):
        ws.column_dimensions[get_column_letter(c)].width = 5.8
    ws.column_dimensions[get_column_letter(20)].width = 5.8

    # row height
#    ws.row_dimensions[2].height = 20
#    for r in range(3, 9):
#        ws.row_dimensions[r].height = 90

    # header
    timestamp = datetime.datetime.today().strftime('%d %B %Y %H:%M')
    ws.header_footer.left_header.text = 'ตารางสอนภาค'+semester+' '+year+' ปรับปรุงล่าสุดเมื่อ '+timestamp
    ws.header_footer.left_header.font_size = 8
    ws.header_footer.center_header.text = instructor
    ws.header_footer.right_header.text = 'มีปัญหาติดต่อ รศ.ดร.ยศนันต์ มีมาก'
    ws.header_footer.right_header.font_size = 8

    return

def printRoom(entryList, room, workbook):
    ws = workbook.create_sheet()

    # set sheet name
    ws.title = room.replace('/', '-')

    # cell alignment
    alignment=Alignment(horizontal='center',
                        vertical='center',
                        wrap_text=True,
                        shrink_to_fit=True)

    # cell border
    thinBorder = Border(left=Side(style='thin'),
                         right=Side(style='thin'),
                         top=Side(style='thin'),
                         bottom=Side(style='thin'))

    # cell font
    font = Font(size=8)

    style = Style(alignment=alignment, font=font, border=thinBorder)

    for r in range(2,9):
        for c in range(1, 21):
            cell = ws.cell(row=r, column=c)
            cell.style = style

    row = 1
    cell = ws.cell(row=1, column=1)
    cell.value = room
    cell.style = cell.style.copy(font=Font(bold=True))

    row = row + 1
    headrow = row

    cell = ws.cell(row=headrow, column=1)
    cell.value = 'วัน/เวลา'

    for c in range(1, 10):
        ws.merge_cells(start_row=headrow-1, start_column=c*2-1, end_row=headrow-1, end_column=c*2)
        cell = ws.cell(row=headrow, column=c*2)
        cell.value = str(c+7)+':00-'+str(c+8)+':00'
    cell = ws.cell(row=headrow, column=20)
    cell.value = '18:00-21:00'

    contentrow = headrow + 1
    for day in {'MO', 'TU', 'WE', 'TH', 'FR', 'AR'}:
        cell = ws.cell(row=contentrow + decodeDay(day), column=1)
        cell.value = day

    rows = [['' for i in range(0, 20)] for j in range(0, 6)]

    term = entryList[0]['Term']
    year = term[1:3]
    if term[3] == '1':
        semester = 'ต้น'
    elif term[3] == '2':
        semester = 'ปลาย'
    elif term[3] == '3':
        semester = 'ฤดูร้อน'
    
    for entry in entryList:
        for day in entry['Meeting Day']:
            timeSlot = getTime(entry['Meeting Time'])
            daySlot = decodeDay(day)
            if daySlot >= 5 or len(timeSlot) == 0:
                rows[5][0] += ' '+entry['Catalog Number']
            else:
                for time in timeSlot:
                    rows[daySlot][time] += entry['Catalog Number']

                    if len({item['Section']
                        for item in datamap if item['Catalog Number'] == entry['Catalog Number']}) > 1:
                            rows[daySlot][time] += ' ('+entry['Section']+')'
#                    rows[daySlot][time] += '\n'+str(entry['Instructor'])+'\n'

    for r in range(0, len(rows)):
        for c in range(0, len(rows[r])):
            cell = ws.cell(row=contentrow + r, column=c+2)
            cell.value = rows[r][c].strip()

    # landscape A4 paper and fit to width
    ws.page_setup.orientation = ws.ORIENTATION_LANDSCAPE
    ws.page_setup.paperSize = ws.PAPERSIZE_A4
    ws.page_setup.fitToWidth = 1
    ws.page_setup.fitToHeight = 1

    # Margin left,right = 0.2 inches, top,bottom = 0.75 inches
#    ws.page_margins.left = 0.2
#    ws.page_margins.right = 0.2
#    ws.page_margins.top = 0.5
#    ws.page_margins.bottom = 0.5
#    ws.page_margins.header = 0.25

    # column width
    ws.column_dimensions[get_column_letter(1)].width = 5
    for c in range(2, 20):
        ws.column_dimensions[get_column_letter(c)].width = 5.8
    ws.column_dimensions[get_column_letter(20)].width = 5.8

    # row height
#    ws.row_dimensions[2].height = 20
#    for r in range(3, 9):
#        ws.row_dimensions[r].height = 90

    # header
    timestamp = datetime.datetime.today().strftime('%d %B %Y %H:%M')
    ws.header_footer.left_header.text = 'ตารางสอนภาค'+semester+year+' ปรับปรุงล่าสุดเมื่อ '+timestamp
    ws.header_footer.left_header.font_size = 8
    ws.header_footer.center_header.text = room
    ws.header_footer.right_header.text = 'มีปัญหาติดต่อ รศ.ดร.ยศนันต์ มีมาก'
    ws.header_footer.right_header.font_size = 8
    return

#with open('CU_CLASS_SCHEDULE.csv', encoding='tis-620') as f:
with open(sys.argv[1], encoding='tis-620') as f:
    data = [row for row in csv.reader(f, delimiter=',', skipinitialspace=True)]

# last line is <script></script>, it must be deleted
data = data[0:-1]

datamap = [{data[0][i]:data[j][i] for i in range(0, len(data[0]))} for j in range(1, len(data))]

for item in datamap:
#    entry['Catalog Number'] = entry['Catalog Number'][4:]
    item['Meeting Day'] = item['Meeting Day'].split(' ')
    item['Instructor'] = {instructor.strip() for instructor in item['Instructor'].split(',')}

instructorSet = {person.strip() for entry in datamap for person in entry['Instructor'] if person is not ''}
instructorList = sorted(list(instructorSet))

# Write timetable for each instructor
schedulebook = Workbook()

# we always get the first blank sheet that should be removed after finished
ws = schedulebook.active

for instructor in instructorList:
    print(instructor)
    printSchedule(datamap, instructor, schedulebook)

# remove the first blank sheet
schedulebook.remove_sheet(ws)
schedulebook.save('schedule.xlsx')


# Write timetable for each room
roombook = Workbook()

roomSet = {entry['Room'] for entry in datamap if entry['Room'] is not ''}
roomList = sorted(list(roomSet))

# we always get the first blank sheet that should be removed after finished
ws = roombook.active

for room in roomList:
    print(room)
    itemList = [entry for entry in datamap if room in entry['Room']]
    printRoom(itemList, room, roombook)

# remove the first blank sheet
roombook.remove_sheet(ws)
roombook.save('room.xlsx')
